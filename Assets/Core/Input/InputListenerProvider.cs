﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Input
{
	public class InputListenerProvider
	{
		public IInputListener InputListener { get; private set; }

		private const string MobileInputResourcePath = "Input/MobileInputListener";
		private const string StandaloneInputResourcePath = "Input/StandaloneInputListener";

		public InputListenerProvider()
		{
#if UNITY_EDITOR
			InputListener =
					Object.Instantiate(Resources.Load<StandaloneInputListener>(StandaloneInputResourcePath));
#endif
#if !UNITY_EDITOR
			_inputListener =
					Object.Instantiate(Resources.Load<MobileInputListener>(MobileInputResourcePath));
#endif
		}
	}
}