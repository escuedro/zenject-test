﻿using UnityEngine;

namespace Core.Input
{
	public class StandaloneInputListener : MonoBehaviour, IInputListener
	{
		private Vector2 _inputDirection;

		public Vector2 InputDirection()
		{
			return _inputDirection;
		}

		public void ReadInput()
		{
			_inputDirection = new Vector2(UnityEngine.Input.GetAxis("Horizontal"),
					UnityEngine.Input.GetAxis("Vertical"));
		}
	}
}