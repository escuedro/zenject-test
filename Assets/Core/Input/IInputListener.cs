using UnityEngine;

namespace Core.Input
{
	public interface IInputListener
	{
		Vector2 InputDirection();
		void ReadInput();
	}
}
