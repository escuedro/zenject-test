﻿using UnityEngine;
using Zenject;

namespace Core.Input
{
	public class MobileInputListener : MonoBehaviour, IInputListener
	{
		private Joystick _joystick;
		private Vector2 _inputDirection;

		[Inject]
		private void Init(Joystick joystick)
		{
			_joystick = joystick;
			_joystick.gameObject.SetActive(true);
		}
		public Vector2 InputDirection()
		{
			return _inputDirection;
		}

		public void ReadInput()
		{
			_inputDirection = _joystick.Direction;
		}
	}
}