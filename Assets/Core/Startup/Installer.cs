using Core.Input;
using UnityEngine;
using Zenject;

namespace Core.Startup
{
    public class Installer : MonoInstaller
    {
        [SerializeField]
        private Joystick _joystick;



        public override void InstallBindings()
        {
            Container.Bind<Joystick>().FromInstance(_joystick).AsSingle();
            BindInputListener();
        }

        private void BindInputListener()
        {
            Container.Bind<InputListenerProvider>()
                    .FromNew()
                    .AsSingle();
        }
    }
}