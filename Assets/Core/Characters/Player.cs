﻿using Core.Input;
using UnityEngine;
using Zenject;

namespace Core.Characters
{
	public class Player : MonoBehaviour
	{
		private IInputListener _inputListener;

		[Inject]
		public void Init(InputListenerProvider inputProvider)
		{
			_inputListener = inputProvider.InputListener;
		}

		private void Update()
		{
			_inputListener.ReadInput();
			transform.position += (Vector3)_inputListener.InputDirection() * (Time.deltaTime * 10f);
		}
	}
}